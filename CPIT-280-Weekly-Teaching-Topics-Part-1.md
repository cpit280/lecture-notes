# CPIT-280 Weekly Teaching Topics

### Week 1. Introduction: Interaction Design and Human-Computer Interaction - Beyond HCI

- What is interaction design?
- What is Human-Computer Interaction (HCI)? Where it came from? How is HCI related to other fields?
- The difference between Interaction Design and Human-Computer Interaction.
- What is involved in the process of interaction design?
- The goals of interaction design.
- Good and bad design examples. 
- The characteristics of good and bad designs.
- Thinking beyond the desktop:
  - Ubiquitous computing.
  - Crowdsourcing
  - Social networks
  - Computer-supported cooperative work (CSCW)
  - Natural user interfaces (e.g., voice assistants)
  - Virtual reality (VR)
  - Brain–computer interface (BCI) 
- What is involved in the process of interaction design?
  - Understanding users' needs.
  - What needs to be taken into account to better understand users' needs.
  - Prototyping and developing alternatives.
  - Evaluation using a user-centered approach.
- Working with a multidisciplinary team.
  - Pros and Cons of working with a diverse multidisciplinary team.
  - Examples: IDEO's [deep dive approach](https://www.ideo.com/post/reimagining-the-shopping-cart) and [video](https://www.youtube.com/watch?v=2Dtrkrz0yoU)
- Accessibility, inclusiveness, and supporting people with disability.

### Week 2-3. Usability of Interactive Systems:	Understanding usability and its standards. Discussion on how to measure usability

* The User-centered design (UCD): is an approach to designing and building interactive systems that aims to make systems usable and useful by focusing on the deep understanding of users and their needs.
* Design Process Guidelines.
* Usability goals:
    - Effectiveness
    - Efficiency
    - Safety
    - Utility
    - Learnability
    - Memorability
* User experience (UX) goals: 
    - Satisfying
    - Enjoyable
    - Entertaining, fun, pleasurable and engaging.
    - Aesthetically beautiful
    - Intuitive: Get what I want in the most simple and direct way. Don't make me think!
* How do usability goals differ from user experience goals?
* Tradeoffs between usability and UX. 
  - Example: the trade-off between usability and security.
* How to measure usability versus user experience goals?
  - What type of questions to ask.
  - Evaluate usability goals of the system.
  - Evaluate UX goals of the system (e.g., user comfort and satisfaction degree with the system).
* Applying techniques on how to measure usability, learnability, memorability, effectiveness and efficiency of a system.
* Visibility of system status. 
* Helping users recover from errors through feedback. 
* Constraints to prevent users from making mistakes.
* Consistency in design	. 
* [Affordances](https://www.interaction-design.org/literature/topics/affordances). What are Affordances? Donald Norman's use of the term affordance.
* [Shneiderman's Eight Golden rules](https://www.interaction-design.org/literature/article/shneiderman-s-eight-golden-rules-will-help-you-design-better-interfaces)
  - Strive for consistency
  - Cater to universal usability
  - Offer informative feedback
  - Design dialogs to yield closure
  - Prevent errors
  - Permit easy reversal of actions
  - Keep users in control
  - Reduce short-term memory load
* [John Maeda's 10 laws of simplicity](http://lawsofsimplicity.com/): Reduce, Organize, Time, Learn, Differences, Context, Emotion, Trust, Failure, The One.
* Four Pillars for a successful interface: 
  - User Interface Requirements
  - Guidelines document and process
  - User-Interface software Tools
  - Expert Reviews and Usability Testing
* Theories vs Principles vs Guidelines
      - Guidelines: Low-level rules and agreed-upon standards that are either advices for good practices and cautions against bad ones. Examples: Google's Material Design Guidelines for Android and Apple's Human Interface Guidelines.
      - Principles: Mid-level rules that are essentially guidelines but more widely applicable than guidelines and treated as a fact for the type of interface being designed. Examples: principles to analyze and compare design principles, determine user’s skill levels and perform task analysis.
      - Theories: High-level explanations and interpretations of why or how a design should work. Examples: explanatory, predictive, cognitive (e.g., cognitive load theory) and performance theories.
* Applying guidelines, principles, golden rules, laws of simplicity and following the 4 Pillar's approach to design user friendly, effective and efficient systems.
* How to identify the target user population/community?
* Defining  the  characteristics  of  the  user  population:
    - Personal and demographic characteristics: Age, sex, education, lifestyle, physical  abilities and constraints (e.g., poor eyesight, color blindness, etc.)
    - Task related characteristics: Goals and motivation, tasks and usage  (heavy  vs.  light,  frequent vs occasionally).
    - Geographic and social characteristics: location (region, country), cultures, social factors.
    - Brainstorming who might be our potential users.
    - Working with a representative sample of the user population.
* Measuring usability goals at scale:
  - Learnability
  - Flexibility and Efficiency of use
  - Recognize, diagnose and recover from errors.
  - Usage patterns.
  - User Retention
  - Satisfaction
* Striving for standardization, tools integration, UI consistency and system portability.
* Automation and human control


### Week 4. User Centered Design 

- Design Process
  - Requirements analysis
  - Preliminary design, architectural or conceptual design, low-fidelity and high-fidelity prototypes
  - Building and implementing the prototypes
  - Evaluation
- [User-centered design (UCD)](https://www.interaction-design.org/literature/topics/user-centered-design): User-centered design (UCD) is an iterative design process in which designers focus on the users and their needs in each phase of the design process.
- Participatory Design: The active involvement of all stakeholders including target users in the design process to help make the final product usable and meets their needs.
- Ethnographic Observation and Participatory Design.
 - Building a rapport.
 - Simplifying your language and terms in order to facilitate understanding between the designer (you) and the users (your clients).
- Understanding User Experiences (UX) to design effective interfaces.
- Examine user-centered design approaches to design interfaces utilizing User Experience (UX).
- The use of user interface metaphors. Examples, benefits, and problems.
- Design Frameworks, methods and tools.
- Practices and Patterns.
- Getting user's attention.
- Forms and data entry.
- Process of Interaction Design and Legal Issues.

### Week 5. Prototyping
- Storyboarding
- Prototyping
- Low fidelity Prototyping and High fidelity Prototyping
- Designing a low-fidelity and High fidelity prototypes for a mobile and/or desktop applications
- Wizard-of-Oz prototyping
- Interaction and interface types: 
  - Instructing (e.g., Command line interfaces (CLIs))
  - Direct Manipulation,(e.g., Graphical user interface (GUI))
  - Exploration of a physical environment (e.g., Virtual reality (VR))
  - Natural language/conversing (e.g., chatbots, and Voice User Interfaces (virtual assistants or intelligent personal assistants)).
- [Personas](https://www.interaction-design.org/literature/topics/personas): Creating fictional characters to better understand users’ needs, experiences, and goals.
  - Using personas to build empathy with target users and focus on their needs.
- [User scenarios](https://www.interaction-design.org/literature/topics/user-scenarios): A detailed description of what target users could do with the system, their actions, and their goals.


### Week 6. Data Analysis, Interpretation, and Presentation	
- Collecting quantitative and qualitative data.
  - Understanding users and data gathering
  - Data recording, interviews and questionnaires, 
  - Observations and combined techniques
  - Acquiring user data through:
    - Data recording (e.g., focus groups and interviews)
    - Conducting interviews
    - Questionnaires
    - [User observation](https://www.interaction-design.org/literature/article/how-to-conduct-user-observations)
    - Combined techniques.
- The difference between quantitative and qualitative data and analysis.
  - Quantitative analysis: Numerical values and methods to measure the size, magnitude, or amount of elements and factors.
  - Qualitative analysis: Helps understand the phenomena and the nature of elements and factors that may cause it. It is often represented as themes, patterns or stories.
- Coding qualitative data: The process of labeling and organizing qualitative data to identify different themes and the relationships between them.
- Validation and evaluation of coding qualitative data
-  Analysis on data gathered from questionnaires, interviews and observations.
  -  Commonly available software packages for data analysis
  -  Data visualization: the process of representing data in a visual and meaningful way so that a user can better understand it.


